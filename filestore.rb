require 'sinatra/base'
require 'sequel'
require 'haml'

class Filestore < Sinatra::Base
  Haml::Options.defaults[:format] = :html5
  Haml::Options.defaults[:escape_html] = true

  get "/" do
    @records = DB["SELECT * FROM users ORDER BY id ASC;"]
    haml :home
  end
end
