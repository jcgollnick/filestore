# Filestore project

This is a simple CRUD app that I can `curl` to and from to keep files
stored remotely without neeting to set up a whole git repository for it.

Supports upload, modify, download, delete of simple files.
